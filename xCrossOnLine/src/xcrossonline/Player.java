/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class Player implements Serializable{
    private final String name;
    private final String symbol;

    public String getSymbol() {
        return symbol;
    }
    
    

    public Player(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }
    


    @Override
    public String toString() {
        return this.name;
    }
    
    
}
