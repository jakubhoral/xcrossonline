/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Server implements Observer {

    private ServerSocket serverSoc;
    private boolean clientConnected = true;

    BufferedWriter output;
//    ObjectOutputStream objOut;
//    ObjectInputStream objIn;
    GameWindow gw;
     Thread gaw;

    public Server(int port, HandShakeWindow hsw) {
        try {
            this.serverSoc = new ServerSocket(port);
            System.out.println("Server was created");
            hsw.getjLabelInfo().setText("Waiting for Client");
            hsw.getjButtonServer().setEnabled(false);
            hsw.getjButtonClient().setEnabled(false);
            Socket clientSocket = this.serverSoc.accept();
            hsw.setVisible(false);
            System.out.println("Client was connected to server");
            Game game = new Game(new Player("server", "x"), new Player("client", "o"));
            gw = new GameWindow(game);
            gw.addObserver(this);
            gaw = new Thread(gw);
            gaw.start();

//            this.objOut= new ObjectOutputStream(clientSocket.getOutputStream());
//            this.objIn= new ObjectInputStream(clientSocket.getInputStream());
            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
//            output.write("Welcome on server\n");
//            output.flush();

            while (clientConnected) {

                String in = input.readLine();
                System.out.println("Coords " + in + " received from Client.");
                game.setButtonFromCoords(in.trim());
                gw.recountButtons(game);
                gw.activeButtons(true);
            }

            input.close();
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        new Server(9000, new HandShakeWindow());
    }

    @Override
    public void update(Observable o, Object arg) {

        try {
            String outCoords = (String) arg;
            output.write(outCoords+"\n");
            output.flush();
            System.out.println("Button coords " + outCoords + " client sent.");
            gw.activeButtons(false);

        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
