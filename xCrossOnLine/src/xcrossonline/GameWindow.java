/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Observable;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Admin
 */
public class GameWindow extends Observable implements Runnable, Serializable {

    final int SIZE = 20;
    JPanel windowContent;
    JPanel xCrossWin;
    JPanel menu;
    JTextField textField;
    JButton quit;
    Game game;
    JFrame frame;
    JLabel playerLabel;

    public GameWindow(Game game) {
        this.game = game;

    }

    @Override
    public void run() {

        frame = new JFrame("Criss Cross");
        windowContent = new JPanel(new BorderLayout());
        menu = new JPanel(new GridLayout(2, 1));
        quit = new JButton("Quit");
        playerLabel = new JLabel(this.game.getPlayer().toString());
        windowContent.add("East", menu);
        menu.add(playerLabel);
        menu.add(quit);
        xCrossWin = new JPanel(new GridLayout(SIZE, SIZE));
        xCrossWin.setSize(300, 300);
//        xCrossWin.setBackground(Color.red);
        this.recountButtons(game);

        xCrossWin.revalidate();
        xCrossWin.repaint();

        windowContent.validate();
        windowContent.add("West", xCrossWin);
        windowContent.revalidate();
        frame.validate();
        windowContent.repaint();
        frame.repaint();
        quit.addActionListener((e) -> {
            System.exit(0);
        });
        frame.setContentPane(windowContent);
        frame.pack();
        frame.setSize(800, 800);
        frame.setVisible(true);

    }

    public Game getGame() {
        return game;
    }

    public void activeButtons(boolean active) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                this.game.getGameButtons()[i][j].setEnabled(active);
            }
        }
        if (active) {
            checkWinner(this.game.getOponent());
        }
    }

    public void checkWinner(Player player) {
        if (this.game.checkWinner(player)) {
            JOptionPane.showMessageDialog(frame, "And the WINNER is:\nPlayer " + player);
            System.exit(0);
        }
    }

    public void recountButtons(Game game) {
        xCrossWin.removeAll();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                xCrossWin.add(this.game.getGameButtons()[i][j]);
                this.game.getGameButtons()[i][j].setVisible(true);
                this.game.getGameButtons()[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        GameButton button = ((GameButton) e.getSource());
                        if (button.getText().equals("")) {
                            button.getGame().setButton(button,
                                    button.getGame().getPlayer());
                            setChanged();
                            notifyObservers(button.getGame().getButtonCoords(button));
                            if (button.getGame().checkWinner(button.getPlayer())) {
                                checkWinner(button.getPlayer());
                            }
                        }
                    }
                });
            }
        }
        frame.setSize(900, 900);
        frame.repaint();
    }
}
