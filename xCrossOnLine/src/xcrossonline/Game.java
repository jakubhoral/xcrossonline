/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Admin
 */
public class Game {

    private Player player;
    private Player oponent;
    private final Player playerZero;
    private GameButton[][] gameButtons;
    private final int SIZE = 20;
    GameWindow gw;

    public Game(Player player, Player oponent) {
        this.player = player;
        this.oponent = oponent;
        this.playerZero = new Player("zero", "");
        gameButtons = new GameButton[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                this.gameButtons[i][j] = new GameButton(i, j, this, this.playerZero);
            }
        }
    }


    public boolean checkWinner(Player player) {
        for (int i = 0; i < this.gameButtons.length; i++) {
            for (int j = 0; j < this.gameButtons[i].length; j++) {
                if (this.gameButtons[i][j].getPlayer().getSymbol().equals(player.getSymbol())) {
                    int countr = 0;
                    int countc = 0;
                    int countdDown = 0;
                    int countdUp = 0;
                    int o = 0;
                    while (o < 5) {
//                            kontrola rady
                        if ((j + o) < this.gameButtons.length) {
                            if (this.gameButtons[i][j + o].getPlayer().getSymbol().equals(player.getSymbol())) {
                                countr++;
                            }
                        }
//                            kontrola sloupce
                        if ((i + o) < this.gameButtons.length) {
                            if (this.gameButtons[i + o][j].getPlayer().getSymbol().equals(player.getSymbol())) {
                                countc++;
                            }
                        }
//                            kontrola diagonalne dolu
                        if ((i + o) < this.gameButtons.length && (j + o) < this.gameButtons.length) {
                            if (this.gameButtons[i + o][j + o].getPlayer().getSymbol().equals(player.getSymbol())) {
                                countdDown++;
                            }
                        }
//                            kontrola diagonalne nahoru
                        if ((i + o) < this.gameButtons.length && (j - o) > 0) {

                            if (this.gameButtons[i + o][j - o].getPlayer().getSymbol().equals(player.getSymbol())) {
                                countdUp++;
                            }
                        }
                        o++;
                    }
                    if (countr > 4 || countc > 4 || countdUp > 4 || countdDown > 4) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public GameButton[][] getGameButtons() {
        return gameButtons;
    }

    public void setGameButtons(GameButton[][] gameButtons) {
        this.gameButtons = gameButtons;
    }

    public boolean setButton(GameButton button, Player player) {
        for (int i = 0; i < this.gameButtons.length; i++) {
            for (int j = 0; j < this.gameButtons[i].length; j++) {
                if (button == gameButtons[i][j]) {
                    gameButtons[i][j].setPlayer(player);
                    return true;
                }
            }
        }
        return false;
    }

    public String getButtonCoords(GameButton button) {
        return button.x + ";" + button.y;
    }

    public void setButtonFromCoords(String coords) {
        int x = Integer.parseInt(coords.split(";")[0]);
        int y = Integer.parseInt(coords.split(";")[1]);
        gameButtons[y][x].setPlayer(oponent);
    }

    public Player getPlayerZero() {
        return playerZero;
    }

    public Player getPlayer() {
        return player;
    }

    public Player getOponent() {
        return oponent;
    }

}
