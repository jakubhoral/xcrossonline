/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.io.Serializable;
import javax.swing.JButton;

/**
 *
 * @author Admin
 */
public class GameButton extends JButton implements Serializable{
    int y;
    int x;
    Game game;
    Player player;


    public GameButton(int y, int x, Game game, Player player) {
        this.y = y;
        this.x = x;
        this.game = game;
        this.player = player;
        super.setName("");
    }

    public Game getGame() {
        return game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        super.setText(player.getSymbol());
        this.player = player;
    }

    
    
}
