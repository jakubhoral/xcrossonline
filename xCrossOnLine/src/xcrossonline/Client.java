/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xcrossonline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Client implements Observer {

    private Socket clientSoc;
    private Scanner userInput;
    private boolean clientRunning = true;
    BufferedWriter output;

    public Client(String ip, int port, HandShakeWindow hsw) {
        try {
            this.clientSoc = new Socket(ip, port);
            hsw.setVisible(false);
            System.out.println("Client was connected to server");
            Game game = new Game(new Player("client", "o"), new Player("server", "x"));
            GameWindow gw = new GameWindow(game);
            gw.addObserver(this);
            new Thread(gw).start();
            gw.activeButtons(false);
            BufferedReader input = new BufferedReader(new InputStreamReader(this.clientSoc.getInputStream()));
            this.output = new BufferedWriter(new OutputStreamWriter(this.clientSoc.getOutputStream()));
            while (clientRunning) {
                String in = input.readLine();
                System.out.println("Coords " + in + " received from server.");
                game.setButtonFromCoords(in.trim());
                gw.recountButtons(game);
                gw.activeButtons(true);
            }
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        new Client("127.0.0.1", 9000, new HandShakeWindow());
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            String outCoords = (String) arg;
            output.write(outCoords+"\n");
            System.out.println("Button coords " + outCoords + " client sent.");
            output.flush();
            ((GameWindow)o).activeButtons(false);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
